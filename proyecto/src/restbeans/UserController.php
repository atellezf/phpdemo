<?php
namespace Utel\RestBeans;

# use Psr\Http\Message\ServerRequestInterface as Request;
# use Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Container\ContainerInterface;
use Utel\DataSource\Usuario;
use Utel\Util\Config;

class UserController {

    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function findById(Request $request, Response $response, array $args) {
        $dbcon = Config::getConnection();
        if(isset($dbcon)) {
            $pstm = $dbcon->prepare(Usuario::SQL_SELECT_ALUMNO_BY_ID);
            $pstm->bindValue(1, $args['id']);
            $pstm->execute();
            $pstm->setFetchMode(\PDO::FETCH_CLASS, Usuario::class);
            $usuario = $pstm->fetch();
            return $response->withJson($usuario);
        }
        $response->getBody()->write("Error de conexión a bd");
        return $response;
    }

    public function insert(Request $request) {
        $dbcon = Config::getConnection();
        if(isset($dbcon)) {
            $parsedBody = $request->getParsedBody();
            $pstm = $dbcon->prepare(Usuario::SQL_INSERT_ALUMNO);
            $pstm->bindValue(1, $parsedBody['nombre']);
            $pstm->bindValue(2, $parsedBody['apellidos']);
            $pstm->bindValue(3, $parsedBody['email']);
            $pstm->bindValue(4, $parsedBody['username']);
            // Se utiliza bindValue debido a que se inserta el resultado
            // de la función md5() y no la variable directamente
            $pstm->bindValue(5, md5( $parsedBody['password'] ));
            $pstm->execute();
        }
    }

}
?>