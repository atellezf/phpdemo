<?php session_start();
require __DIR__ . '/../../vendor/autoload.php';

## CONFIGURE SLIM FRAMEWORK

# Instantiate the app
$app = new \Slim\App();

# Register routes
use Utel\RestBeans\UserController;
$app->get('/usuario/{id}', UserController::class . ':findById');
$app->post('/usuario', UserController::class . ':insert');

# Run app
$app->run();

?>