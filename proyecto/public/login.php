<?php session_start();
    require __DIR__ . '/../vendor/autoload.php';
    use Utel\Util\Config;
    use Utel\DataSource\Usuario;

    if(isset($_SESSION['authuser'])) {
        header('Location: index.php');
    }

    $errores = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
        $password = md5($_POST['password']);

        $dbcon = Config::getConnection();
        $sentencia = $dbcon->prepare(Usuario::SQL_SELECT_LOGIN_USUARIO);
        $sentencia->bindParam(1, $usuario);
        $sentencia->bindParam(2, $password);
        $sentencia->execute();

        $resultado = $sentencia->fetch();
        if($resultado !== false) {
            $_SESSION['authuser'] = $usuario;
            header('Location: index.php');
        } else {
            $errores .= '<li>Datos Incorrectos</li>';
        }

    }

    require Config::getView('login.view.php');
?>