<?php session_start();
require __DIR__ . '/../vendor/autoload.php';

use Utel\Util\Config;
use Utel\DataSource\Usuario;

if(!isset($_SESSION['authuser'])) {
    header('Location: login.php');
}

if(($_SERVER['REQUEST_METHOD']=='GET') and isset($_GET['id'])) {
    $dbcon = Config::getConnection();
    $pstm = $dbcon->prepare(Usuario::SQL_SELECT_ALUMNO_BY_ID);
    $pstm->bindValue(1, $_GET['id']);
    $pstm->execute();
    $pstm->setFetchMode(PDO::FETCH_CLASS, Usuario::class);
    $alumno = $pstm->fetch();
} else if($_SERVER['REQUEST_METHOD']=='POST') {
    $id = $_POST['id'];
    $opcion = $_POST['opcion'];
    $dbcon = Config::getConnection();
    if(($opcion=='ok') and $dbcon) {
        try {
            $pstm = $dbcon->prepare(Usuario::SQL_DELETE_ALUMNO);
            $pstm->bindParam(1, $id);
            $pstm->execute();
        } catch(PDOException $e) {
            Config::getLogger()->error($e->getMessage());
        }
    }
    header('Location: index.php');

} else {
    # EN CASO DE QUE EL USUARIO ACCEDIERA A ESTA PAGINA ESCRIBIENDO LA URL, LO REGRESAMOS AL INICIO
    header('Location: index.php');
}

require Config::getView('eliminar.view.php');
?>
