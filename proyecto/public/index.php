<?php session_start();
require __DIR__ . '/../vendor/autoload.php';
use Utel\Util\Config;
use Utel\DataSource\Usuario;

if(!isset($_SESSION['authuser'])) {
    header('Location: login.php');
}

$dbcon = Config::getConnection();
if(isset($dbcon)) {
    $sentencia = $dbcon->prepare(Usuario::SQL_SELECT_ALUMNOS);
    $sentencia->execute();
    $usuarios = $sentencia->fetchAll(PDO::FETCH_NAMED);
}


require Config::getView('index.view.php');
?>