<?php session_start();
require __DIR__ . '/../vendor/autoload.php';

use Utel\Util\Config;
use Utel\DataSource\Usuario;

if(!isset($_SESSION['authuser'])) {
    header('Location: login.php');
}

if(($_SERVER['REQUEST_METHOD']=='GET') and isset($_GET['id'])) {
    $dbcon = Config::getConnection();
    $pstm = $dbcon->prepare(Usuario::SQL_SELECT_ALUMNO_BY_ID);
    $pstm->bindValue(1, $_GET['id']);
    $pstm->execute();
    $resultado = $pstm->fetch(PDO::FETCH_NAMED);
    extract($resultado);
} else if($_SERVER['REQUEST_METHOD']=='POST') {
    extract($_POST);
    $nombre = filter_var($nombre, FILTER_SANITIZE_STRING);
    $apellidos = filter_var($apellidos, FILTER_SANITIZE_STRING);
    $username = filter_var($username, FILTER_SANITIZE_STRING);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $errores = "";
    if(Config::camposVacios($nombre, $apellidos, $username, $email, $password, $password2)) {
        $errores .= '<li class="list-group-item text-danger">Por favor rellena todos los datos correctamente</li>';
    } else if($password != $password2) {
        $errores .= '<li class="list-group-item text-danger">Las contraseñas no son iguales</li>';
    } else {
        $dbcon = Config::getConnection();
        if($dbcon) {
            try {
                $pstm = $dbcon->prepare(Usuario::SQL_UPDATE_ALUMNO);
                $pstm->bindParam(1, $nombre);
                $pstm->bindParam(2, $apellidos);
                $pstm->bindParam(3, $email);
                $pstm->bindParam(4, $username);
                // Se utiliza bindValue debido a que se inserta el resultado
                // de la función md5() y no la variable directamente
                $pstm->bindValue(5, md5($password));
                $pstm->bindParam(6, $id);
                $pstm->execute();
            } catch(PDOException $e) {
                Config::getLogger()->error($e->getMessage());
            }
            header('Location: index.php');
        }
    }
    
} else {
    # EN CASO DE QUE EL USUARIO ACCEDIERA A ESTA PAGINA ESCRIBIENDO LA URL, LO REGRESAMOS AL INICIO
    header('Location: index.php');
}

require  Config::getView('editar.view.php');
?>
